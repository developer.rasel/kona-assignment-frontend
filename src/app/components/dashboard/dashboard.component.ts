import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {map, Observable} from "rxjs";
import {DashboardService, LiveScoreData} from "../../services/dashboard.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  filterValue: any = null;
  dataSource: any = null;
  pageEvent: any;

  displayedColumns: string[] = ['id', 'title', 'description', 'guid', 'link'];
  constructor(private dashboardService: DashboardService, private router: Router, private activatedRoute: ActivatedRoute,private auth:AuthService) { }

  ngOnInit(): void {
    this.initDataSource();
  }

  initDataSource() {
    this.dashboardService.paginateByKeyword(0, 10, "").pipe(
      map((liveScoreData: LiveScoreData) => this.dataSource = liveScoreData)
    ).subscribe()
    console.log("success response:"+this.dataSource);

  }

  onPaginateChange(event: PageEvent) {
    let page = event.pageIndex;
    let size = event.pageSize;


    if(this.filterValue == null) {
      page = page +1;
      this.dashboardService.paginateByKeyword(page, size,"").pipe(
        map((liveScoreData: LiveScoreData) => this.dataSource = liveScoreData)
      ).subscribe();
    } else {
      this.dashboardService.paginateByKeyword(page, size, this.filterValue).pipe(
        map((liveScoreData: LiveScoreData) => this.dataSource = liveScoreData)
      ).subscribe()
    }

  }

  findByKeyword(keyword: string) {
    console.log("search key:"+keyword);
    this.dashboardService.paginateByKeyword(0, 10, keyword).pipe(
      map((liveScoreData: LiveScoreData) => this.dataSource = liveScoreData)
    ).subscribe()
  }

  public logOut() {
    this.auth.logOut();
    // window.location.href='/login';
    this.router.navigate(['/login'])
  }

}
