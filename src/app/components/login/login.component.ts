import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {TokenStorageService} from "../../services/token-storage.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginData={
    username:"",
    password:""
  }

  constructor(private snack: MatSnackBar,private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }


  formSubmit() {
    if (this.loginData.username.trim()=='' || this.loginData.username==null){
      this.snack.open("Username is required","close",{duration:3000});
      return;
    }
    if (this.loginData.password.trim()=='' || this.loginData.password==null){
      this.snack.open("Password is required","close",{duration:3000});
      return;
    }

    this.auth.generateToken(this.loginData).subscribe(
      (data:any)=>{
        if (data.data!=null){
          this.auth.authenticUser(data.data.token);
          this.auth.setUser(data.userEntity);
          this.snack.open("success!","OK",{duration:3000})
          if (this.auth.isLoggedIn() == true){
            // window.location.href="/dashboard";
            this.router.navigate(['/dashboard']);
          }else {
            this.auth.logOut();
          }
          if (data.userEntity!=null){
            // window.location.href='/dashboard'
            this.router.navigate(['/dashboard']);
          }else {
            this.auth.logOut();
          }
        }else {
          this.snack.open(data.message,"OK",{duration:3000});
        }

      },
      (error => {
        // console.log(error);
        this.snack.open("Something went wrong!","Close",{duration:3000});
      })
    );
  }

}
