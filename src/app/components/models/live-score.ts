export interface LiveScore {
  id: number;
  title: string;
  description: string;
  guid: string;
  link: string;
  // createDate: number;
  // updateDate?: any;
}
