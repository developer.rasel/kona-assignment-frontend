export interface Sort2 {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}
