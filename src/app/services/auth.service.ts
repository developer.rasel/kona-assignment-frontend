import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const AUTH_API = 'http://localhost:8084/api/auth/login';
const REFRESH_TOKEN_API = 'http://localhost:8084/api//auth/refresh-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  public generateToken(logindata:any){
    return this.http.post(AUTH_API,logindata);
  }

  public authenticUser(token:any){
    localStorage.setItem("jwtToken",token);
    return true;
  }

  public isLoggedIn(){
    let tokenStr=localStorage.getItem("jwtToken");
    if (tokenStr == undefined || tokenStr=='' || tokenStr==null){
      return false;
    }else {
      return true;
    }
  }

  public logOut(){
    localStorage.removeItem("jwtToken");
    localStorage.removeItem("user");
    return true;
  }

  public getToken(){
    return localStorage.getItem("jwtToken");
  }



  public getUser(){
    let userdata=localStorage.getItem("user");
    if (userdata!=null){
      return JSON.parse(userdata);
    }else {
      this.logOut();
      return null;
    }
  }

  public setUser(user:any){
    localStorage.setItem("user",JSON.stringify(user));
  }
  public getUserRole(){
    let user=this.getUser();
    return user.authorities[0].authority;
  }

  refreshToken() {
    this.http.post(REFRESH_TOKEN_API,localStorage.getItem('jwtToken'));
  }
}
