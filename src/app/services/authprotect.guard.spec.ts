import { TestBed } from '@angular/core/testing';

import { AuthprotectGuard } from './authprotect.guard';

describe('AuthprotectGuard', () => {
  let guard: AuthprotectGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthprotectGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
