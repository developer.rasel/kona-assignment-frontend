import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {LiveScore} from "../components/models/live-score";
import {Pageable} from "../components/models/pageable";
import {Sort2} from "../components/models/sort2";


export interface LiveScoreData {
  content: LiveScore[];
  pageable: Pageable;
  totalElements: number;
  last: boolean;
  totalPages: number;
  size: number;
  number: number;
  sort: Sort2;
  numberOfElements: number;
  first: boolean;
  empty: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  paginateByKeyword(page: number, size: number, search: string): Observable<LiveScoreData> {
    let params = new HttpParams();

    params = params.append('draw', 1);
    params = params.append('start', 0);
    params = params.append('length', String(size));
    params = params.append('search', search);

    return this.http.get('http://localhost:8084/api/dashboard/livescore', {params}).pipe(
      map((liveScoreData: any) => liveScoreData),
      catchError(err => throwError(err))
    );


  }


}
